// const { JoinOurProgram } = require(['./join-us-section.js']);
import {JoinOurProgram} from './join-us-section.js'
// const { validate } = require(['./email-validator.js']);
import { validate } from './email-validator.js'

document.addEventListener('DOMContentLoaded', function () {
    const joinOurProgramSection = document.getElementById('join-our-program');
    if(JoinOurProgram)
    joinOurProgramSection.appendChild(JoinOurProgram.createStandardSection);

    const emailInput = document.getElementById('joinOurProgramEmail');
    const submitButton = document.getElementById('submitButton');

    // Populate email input with value from localStorage on page load
    const storedEmail = localStorage.getItem('subscriptionEmail');
    if (storedEmail) {
        emailInput.value = storedEmail;
    }

    // Save email input value to localStorage on input change
    emailInput.addEventListener('input', function () {
        localStorage.setItem('subscriptionEmail', emailInput.value);
    });

    document.getElementById('submitForm').addEventListener('submit', function (event) {
        event.preventDefault();
        const email = emailInput.value.trim();
        const isValid = validate(email);

        if (isValid) {
            // Hide subscription email input and change button to "Unsubscribe"
            emailInput.style.display = 'none';
            submitButton.innerText = 'Unsubscribe';
            submitButton.style.margin = '0 auto';

            // Save UI state to localStorage
            localStorage.setItem('subscriptionState', 'subscribed');
        } else {
            alert('Email is not valid.');
        }
    });

    // Implement functionality to unsubscribe
    submitButton.addEventListener('click', function () {
        const subscriptionState = localStorage.getItem('subscriptionState');
        if (subscriptionState === 'subscribed') {
            // Reset UI state
            emailInput.style.display = 'block';
            emailInput.value = '';
            submitButton.innerText = 'Subscribe';

            // Remove email from localStorage
            localStorage.removeItem('subscriptionEmail');
            // Update subscription state in localStorage
            localStorage.setItem('subscriptionState', 'unsubscribed');
        }
    });
});
