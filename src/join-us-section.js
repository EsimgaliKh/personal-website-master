export function JoinOurProgram () {
    function createSection(titleText, buttonText) {
        
        const section = document.createElement('div');
        section.innerHTML = `
        <section id="newSection" style="background-image: url('./assets/images/photo.png'); text-align: center;width: 1200px; height: 436px; display: flex; flex-direction: column; justify-content: center;">
        <div style="font-size: 48px; font-family: Oswald; color: white;">${titleText}</div>
        <div style="display: flex; justify-content: center; margin-top: 20px; margin-bottom: 30px;">
          <div style="Width: 452px; font-size: 24px; font-family: Source Sans Pro; color:#FFFFFFB2;">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
        </div>
        <div style="display: flex; justify-content: center;">
        
        <form 
        id="submitForm">
          <input
           style="width: 400px;
           height: 31px; 
           margin-right: 20px;
           background-color: #FFFFFF26;
            color: #FFFFFF;
            border: #FFFFFF;" placeholder="Email"
            id="joinOurProgramEmail"
            type="text"/>
          <button style="font-family: Oswald;
          font-size: 12px;
          font-weight: 400;
          line-height: 26px;
          letter-spacing: 0.1em;
          text-align: center;
          width: 111px;
          color:  #FFFFFF;
          border: 0px;
          background-color: #55C2D8;
          border-radius: 20px;
          cursor:pointer;
          type="submit">${buttonText}</button>
          </form>
        </div>
      </section>`

        return section;
    }

    function removeSection() {
        let newSection = document.querySelector("#newSection")
        newSection.style.display = "none"
    }

    return {
        createStandardSection: function () {
            return createSection('Join Our Program', 'SUBSCRIBE');
        },
        createAdvancedSection: function () {
            return createSection('Join Our Advanced Program ', 'Subscribe to Advanced Program');
        },
        deleteSection: function(){
            return removeSection()
        }
    };
}
