// const module = require(["module"])
const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

export function validate(email) {
    const emailParts = email !== undefined ? email.split('@') : "";
    if (emailParts.length !== 2) {
        return false;
    }
    const domain = emailParts[1];
    return VALID_EMAIL_ENDINGS.includes(domain);
}
